﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PS.OTT.DRMAdapter.VF.Models
{
    public class DefaultUsageRule
    {
        public int minLevel { get; set; }
        public bool hdcp { get; set; }
        public string hdcpType { get; set; }
        public bool unprotectedDigitalOutput { get; set; }
        public string uncompressedDigitalCappingResolution { get; set; }
    }
    public class DRMSettings
    {
        public string TVPApiUrl { get; set; }
        public string TVPApiUser { get; set; }
        public string TVPApiPassword { get; set; }
        public string TVPApiPlatform { get; set; }
        public string TVPApiAppPassword { get; set; }
        public string TVPApiAppUsername { get; set; }

        public List<DefaultUsageRule> DefaultUsageRules { get; set; }
        public int AuthTTL { get; set; }
        public int ClmTTL { get; set; }
        public string Kid { get; set; }
        public int ClmLicenseSVOD { get; set; }
        public int ClmLicenseDownloadContent { get; set; }
        public Dictionary<string, int> OplDictionary { get; set; }
        public string AuthNagraSecret { get; set; }
        public string ClmNagraSecret { get; set; }
        public int GroupId { get; set; }
        public string PhoenixUrl { get; set; }
        public string AppToken { get; set; }
        public string AppID { get; set; }
        
    }
}