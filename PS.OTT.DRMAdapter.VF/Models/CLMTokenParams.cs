﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PS.OTT.DRMAdapter.VF.Models
{
    public class Device
    {
        public string deviceId { get; set; }
    }

    public class ContentRight
    {
        public string contentId { get; set; }
        public long start { get; set; }
        public long end { get; set; }
        public DefaultUsageRule defaultUsageRules { get; set; }
        public bool? storable { get; set; }
    }

    public class CLMClaims
    {
        public string typ { get; set; }
        public string ver { get; set; }
        public long exp { get; set; }
        public Device device { get; set; }
        public IList<ContentRight> contentRights { get; set; }
    }

    public class CLMTokenParams
    {
        public Header header { get; set; }
        public CLMClaims claims { get; set; }
    }
}