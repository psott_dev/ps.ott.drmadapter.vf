﻿namespace PS.OTT.DRMAdapter.VF.Models
{
    public class Header
    {
        public string typ { get; set; }
        public string alg { get; set; }
        public string kid { get; set; }
    }

    public class Claims
    {
        public string typ { get; set; }
        public string ver { get; set; }
        public long exp { get; set; }
        public string deviceId { get; set; }
    }

    public class AuthTokenParams
    {
        public Header header { get; set; }
        public Claims claims { get; set; }
    }
}
   