﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kaltura.Enums;

namespace PS.OTT.DRMAdapter.VF.BECalls.TVPApiModels
{
    public class InitObject
    {
        public string Token { get; set; }
        public TVPApiLocale Locale { get; set; }
        public string Platform { get; set; }
        public string SiteGuid { get; set; }
        public string UDID { get; set; }
        public string ApiUser { get; set; }
        public int DomainID { get; set; }
        public string ApiPass { get; set; }
    }

    public class TVPApiLocale
    {
        public string LocaleLanguage { get; set; }
        public string LocaleCountry { get; set; }
        public string LocaleDevice { get; set; }
        public string LocaleUserState { get; set; }

        public TVPApiLocale()
        {
            LocaleLanguage = "";
            LocaleCountry = "";
            LocaleDevice = "";
            LocaleUserState = "Unknown";
        }
    }
}