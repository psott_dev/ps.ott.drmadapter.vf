﻿using Kaltura;
using Kaltura.Enums;
using Kaltura.Services;
using Kaltura.Types;
using KLogMonitor;
using PS.OTT.DRMAdapter.VF.Helper;
using System;
using System.Linq;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using Kaltura.Request;

namespace PS.OTT.DRMAdapter.VF.BECalls
{
    public class PhoenixWrapper
    {
        private readonly Kaltura.Client _PhoenixClient;
        private readonly KLogger _Logger;
        private readonly AdapaterConfiguration _HandlerConfig;

        public PhoenixWrapper(AdapaterConfiguration handlerConfig, KLogger logger)
        {
            _HandlerConfig = handlerConfig;
            _Logger = logger;

            _PhoenixClient = new Kaltura.Client(new Configuration
            {
                ServiceUrl = _HandlerConfig.DrmSettings.PhoenixUrl,
                Logger = new PhoenixLoggerKloggerWrapper(_Logger),
            });

            var ksToken = GetPhoenixToken();
            _PhoenixClient.setKS(ksToken);
        }

        public string GetHouseholdByUserId(string userId)
        {
            long householdId = -1;
            try
            {
                var Ks = GetPhoenixToken();

                var user = (OTTUser)OttUserService.Get().ExecuteAndWaitForResponse(_PhoenixClient);
                householdId = user.HouseholdId;
                return householdId >= 0 ? householdId.ToString() : string.Empty;
            }
            catch (Exception e)
            {
                _Logger.Error($"Error> GetHouseholdByUserId> Exception for userId:{userId}. Ex: {e.Message}");
                return null;
            }
        }

        public ProductPrice GetProductOfFile(long fileId, int userId)
        {
            try
            {
                var filter = new ProductPriceFilter { FileIdIn = fileId.ToString(), IsLowest = true };
                var products = ProductPriceService.List(filter).WithUserId(userId).ExecuteAndWaitForResponse(_PhoenixClient);
                var productToReturn = products.Objects.FirstOrDefault();
                if (productToReturn == null) { throw new Exception($"Failed to find relevant product for file:[{fileId}]"); }

                return productToReturn;
            }
            catch (Exception e)
            {
                _Logger.Error($"Error> GetProducts> Exception for fileId:{fileId}, userId: {userId}. Ex: {e.Message}");
                throw;
            }
        }

        public AssetFileContext GetAssetContext(string fileId, int userId)
        {
            try
            {
                var assetResult = AssetFileService.GetContext(fileId, ContextType.NONE).WithUserId(userId).ExecuteAndWaitForResponse(_PhoenixClient);
                return assetResult;
            }
            catch (Exception e)
            {
                _Logger.Error($"Error> GetAssetContext> Exception for fileId:{fileId}, userId: {userId}. Ex: {e.Message}");
                return null;
            }
        }

        public Ppv GetPPVById(long ppvId)
        {
            try
            {
                var ppv = PpvService.Get(ppvId).ExecuteAndWaitForResponse(_PhoenixClient);
                return ppv;
            }
            catch (Exception e)
            {
                _Logger.Error($"Error> GetPPV> Exception for ppvId:{ppvId}. Ex: {e.Message}");
                return null;
            }
        }

        public ProgramAsset GetEpgData(int programId)
        {
            try
            {
                var response = (ProgramAsset)AssetService.Get(programId.ToString(), Kaltura.Enums.AssetReferenceType.EPG_INTERNAL).ExecuteAndWaitForResponse(_PhoenixClient);
                return response;
            }
            catch (Exception e)
            {
                _Logger.Error($"Error> GetEpgData> Exception for programId:{programId}. Ex: {e.Message}");
                return null;
            }
        }

        private string GetPhoenixToken()
        {
            var cacheKeyForKS = $"VF_SSO_KS";
            var cache = MemoryCache.Default;

            _Logger.Info($"searching for KS in cache using key {cacheKeyForKS}");
            var ks = (string)cache[cacheKeyForKS];
            if (ks == null) { _Logger.Info("No KS in cache generating a new one"); }
            ks = ks ?? GenerateNewKS(cacheKeyForKS);
            return ks;
        }

        private string GenerateNewKS(string cacheKeyForKS)
        {
            var cache = MemoryCache.Default;
            _Logger.Info($"SignIn with app token.");

            var anonymousLoginResponse = OttUserService.AnonymousLogin(_HandlerConfig.DrmSettings.GroupId).ExecuteAndWaitForResponse(_PhoenixClient);
            var anonymousKS = anonymousLoginResponse.Ks;
            var tokenToHash = anonymousKS + _HandlerConfig.DrmSettings.AppToken;
            var tokenHash = Sha1Hash(tokenToHash);
            _PhoenixClient.setKS(anonymousKS);
            var loginResponse = AppTokenService.StartSession(_HandlerConfig.DrmSettings.AppID, tokenHash).ExecuteAndWaitForResponse(_PhoenixClient);

            var expiryDate = Utils.FromUnixTime(loginResponse.Expiry);
            var days = (expiryDate - DateTime.UtcNow).Days;
            var policy = new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.UtcNow.AddDays(days) };
            cache.Set(cacheKeyForKS, loginResponse.Ks, policy);
            return loginResponse.Ks;
        }

        private static string Sha1Hash(string value)
        {
            using (var hash = SHA1.Create())
            {
                return string.Concat(hash
                    .ComputeHash(Encoding.UTF8.GetBytes(value))
                    .Select(item => item.ToString("x2")));
            }
        }

        public bool CheckIfAssetIsDownloadable(string assetId)
        {

            var asset = AssetService.Get(assetId, AssetReferenceType.MEDIA).ExecuteAndWaitForResponse(_PhoenixClient);
            var isDownloadableMetaTag = asset.Metas.TryGetValueSafe("isDownloadable ", null);

            _Logger.Debug($"Trying to get meta tag [isDownloadable].. got value of type:[{isDownloadableMetaTag}]");
            var isDownloadable = (isDownloadableMetaTag as BooleanValue)?.Value;

            _Logger.Debug($"Parsed [isDownloadable]=[{isDownloadable}]");

            return isDownloadable == true;
        }

        public Asset GetAsset(string assetId)
        {
            var asset = AssetService.Get(assetId, AssetReferenceType.MEDIA).ExecuteAndWaitForResponse(_PhoenixClient);
            return asset;
        }
    }

    public class PhoenixLoggerKloggerWrapper : ILogger
    {
        private readonly KLogger _Logger;

        public PhoenixLoggerKloggerWrapper(KLogger logger)
        {
            _Logger = logger;
        }

        public void Log(string msg)
        {
            _Logger.Debug(msg);
        }
    }
}