﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using KLogMonitor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PS.OTT.DRMAdapter.VF.BECalls.TVPApiModels;
using PS.OTT.DRMAdapter.VF.Helper;
using RestSharp;

namespace PS.OTT.DRMAdapter.VF.BECalls
{
    public class TVPApiWrapper
    {
        private readonly AdapaterConfiguration _AdapaterConfiguration;
        private readonly KLogger _Logger;
        private readonly RestClient _Client;

        public TVPApiWrapper(AdapaterConfiguration adapaterConfiguration, KLogger logger)
        {
            _AdapaterConfiguration = adapaterConfiguration;
            _Logger = logger;
            _Client = new RestClient(adapaterConfiguration.DrmSettings.TVPApiUrl)
            {
                Timeout = (int)TimeSpan.FromMinutes(5).TotalMilliseconds,
            };
        }

        private InitObject GetInitObject()
        {
            return new InitObject
            {
                Locale = new TVPApiLocale(),
                Platform = _AdapaterConfiguration.DrmSettings.TVPApiPlatform,
                SiteGuid = "",
                ApiUser = _AdapaterConfiguration.DrmSettings.TVPApiUser,
                ApiPass = _AdapaterConfiguration.DrmSettings.TVPApiPassword,
            };
        }

        public string Authorize(string username, string password)
        {
            var request = new RestRequest("gateways/jsonpostgw.aspx?m=AdminSignIn", Method.POST);
            var initObj = GetInitObject();
            initObj.UDID = $"PS.OTT.DRMAdapter.VF.{Assembly.GetExecutingAssembly().GetName().Version}";
            var requestData = new
            {
                initObj = initObj,
                userName = username,
                password = password,
            };

            var response = GetResponse(request, requestData);
            var responseData = JsonConvert.DeserializeObject<dynamic>(response.Content);

            //if (responseData.LoginStatus != 0) { throw new Exception($"Could not SignIn TPVApi user:[{username}] pass:[{password}] login status is not 0"); }

            var tokenResponseHeader = response.Headers.FirstOrDefault(h => h.Name.Equals("access_token", StringComparison.OrdinalIgnoreCase))?.Value?.ToString();

            _Logger.Debug($"Response Token Header, {tokenResponseHeader}");
            if (tokenResponseHeader == null) { throw new Exception($"Could not SignIn TPVApi user:[{username}] pass:[{password}]"); }

            var tokenParts = tokenResponseHeader.Split('|');
            var token = tokenParts[0];
            //var expiryEpoch = tokenParts[1];
            return token;
        }


        public bool CheckIfRecordingAssetIsDownloadable(string recordingId, string userId)
        {
            var request = new RestRequest("gateways/jsonpostgw.aspx?m=GetRecordings", Method.POST);
            var initObj = GetInitObject();
            initObj.SiteGuid = userId;

            var requestData = new
            {
                initObj = initObj,
                searchBy = "ByRecordingID",
                recordingIDs = new[] { int.Parse(recordingId) },
                recordedEPGOrderObj = new { m_eOrderBy = "StartTime", m_eOrderDir = "ASC" }
            };

            var token = Authorize(_AdapaterConfiguration.DrmSettings.TVPApiAppUsername, _AdapaterConfiguration.DrmSettings.TVPApiAppPassword);
            initObj.Token = token;

            var response = GetResponse(request, requestData);
            var responseData = JsonConvert.DeserializeObject<IEnumerable<dynamic>>(response.Content).FirstOrDefault();
            if (responseData == null) { throw new Exception($"Could not find recordingId:[{recordingId}]"); }

            var tags = (JObject)responseData;
            _Logger.Debug($"found the following tags in the recording asset:[{tags.ToJson()}]");
            var genreTag = tags.SelectToken("$.EPG_TAGS[?(@.Key == 'genre')]")?["Value"]?.Value<string>();
            _Logger.Debug($"Genre tag is :[{genreTag}]");
            var contentTags = genreTag.Split(' ');

            var d_g_ContentTag = contentTags.FirstOrDefault(ct => ct.Contains("d_g"));
            var d_g_Value = d_g_ContentTag?.Split('=')[1];
            return d_g_Value?.Equals("1", StringComparison.OrdinalIgnoreCase) == true;
        }

        private IRestResponse GetResponse(IRestRequest request, object requestData)
        {
            request.AddJsonBody(requestData);
            _Logger.Debug($"TVPApi POST: [{_Client.BuildUri(request)}]");
            _Logger.Debug($"TVPApi REQUEST BODY: [{requestData.ToJson()}]");

            var response = _Client.Post(request);
            _Logger.Debug($"TVPApi RESPONSE BODY: [{response.Content.ToJson()}]");
            return response;
        }
    }
}