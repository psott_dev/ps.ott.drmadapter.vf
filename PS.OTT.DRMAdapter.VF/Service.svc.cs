﻿using KLogMonitor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using AdapaterCommon.Models;
using AdapaterCommon.Helpers;
using PS.OTT.DRMAdapter.VF.BECalls;
using Newtonsoft.Json;
using Kaltura.Types;
using Kaltura.Enums;
using PS.OTT.DRMAdapter.VF.Models;
using PS.OTT.DRMAdapter.VF.Helper;
using DRMAdapter;
using AssetType = DRMAdapter.AssetType;
using ContextType = DRMAdapter.ContextType;

namespace PS.OTT.DRMAdapter.VF
{
    public class Service : IService
    {
        private static readonly KLogger _Logger = new KLogger(MethodBase.GetCurrentMethod().DeclaringType.ToString());
        private const string SIGNATURE_MISMATCH = "Signature does not match";

        private AdapaterConfiguration _AdapaterConfiguration;
        private PhoenixWrapper _PhoenixWrapper;
        private TVPApiWrapper _TVPapiWrapper;
        private DRMHelper _DRMLogic;

        public Service()
        {
            _Logger.Debug($"Constructing PS.OTT.DRMAdapter.VF from assembly version: {Assembly.GetExecutingAssembly().GetName().Version}");

        }

        public AdapterStatus SetConfiguration(int adapterId, string settings, int partnerId, long timeStamp, string signature)
        {
            var responseStatus = CommonResponses.Error;

            try
            {
                _Logger.DebugFormat("SetConfiguration : {0}", HttpContext.Current.Request.Url.AbsoluteUri);

                // validate signature
                var isSignatureValid = ValidateSignature(adapterId, signature, settings, partnerId, timeStamp);
                if (!isSignatureValid)
                    return CommonResponses.SignatureMismatch;
                _AdapaterConfiguration = new AdapaterConfiguration(_Logger, partnerId, adapterId);
                _Logger.Debug($"Deserializing settings:[{settings}]");
                var settingsObj = JsonConvert.DeserializeObject<DRMSettings>(settings);

                _AdapaterConfiguration.SetDrmSettings(settingsObj, adapterId);

                responseStatus = CommonResponses.OK;
                return responseStatus;
            }
            catch (Exception ex)
            {
                _Logger.Error("Error in SetConfiguration", ex);
                responseStatus = CommonResponses.Error;
            }
            responseStatus = CommonResponses.Error;
            return responseStatus;
        }

        public DrmAdapterResponse GetAssetLicenseData(int adapterId, int partnerId, string userId, string assetId, AssetType assetType, long fileId, string externalFileId, string ip, string udid, ContextType context, string recordingId, long timeStamp, string signature)
        {
            _Logger.InfoFormat("start GetAssetLicenseData");
            var response = new DrmAdapterResponse() { Status = new AdapterStatus() { Code = (int)AdapterStatusCode.Error } };
            try
            {
                #region check signature, params and config
                // validate signature
                var settingsConcat = string.Concat(adapterId, userId, assetId, assetType, fileId, ip, udid, timeStamp, signature);
                var isSignatureValid = ValidateSignature(adapterId, signature, settingsConcat, timeStamp, context, recordingId);
                if (!isSignatureValid)
                {
                    _Logger.ErrorFormat("GetAssetLicenseData : SignatureMismatch");
                    response.Status = CommonResponses.SignatureMismatch;
                    return response;
                }
                _AdapaterConfiguration = new AdapaterConfiguration(_Logger, partnerId, adapterId);
                // check configurations
                if (_AdapaterConfiguration.IsConfigurationSet(adapterId) == false)
                {
                    _Logger.ErrorFormat("GetAssetLicenseData : No Configuration Found");
                    response.Status = CommonResponses.NoConfigurationFound;
                    return response;
                }

                if (_AdapaterConfiguration.DrmSettings == null)
                {
                    _Logger.Error("Error in GetDeviceLicenseData: drmSettings is null. please Set Configuration.");
                    response.Status = CommonResponses.Error;
                    return response;
                }

                var intUserId = -1;
                int.TryParse(userId, out intUserId);
                if (intUserId == -1)
                {
                    _Logger.ErrorFormat("GetAssetLicenseData : illegal user id {0}", userId);
                    response.Status = CommonResponses.Error;
                    return response;
                }

                #endregion

                _PhoenixWrapper = new PhoenixWrapper(_AdapaterConfiguration, _Logger);
                _TVPapiWrapper = new TVPApiWrapper(_AdapaterConfiguration, _Logger);
                _DRMLogic = new DRMHelper(_AdapaterConfiguration, _Logger, _PhoenixWrapper);

                // Check TVOD\SVOD and set license duration (6Hr for SVOD and PPV Full Lifecycle for PPV)
                var productEndDate = GetProductEndDateForFileId(assetId, recordingId, fileId, intUserId, context, assetType);

                var usageRules = LookupOPLAndCreateDefaultUsageRules(externalFileId, assetId, assetType);

                //Build CLM Token
                var clmToken = _DRMLogic.CreateVODCLMToken(udid, externalFileId, productEndDate, usageRules, context);

                response.Status = CommonResponses.OK;
                response.Data = clmToken;

                return response;
            }
            catch (Exception ex)
            {
                _Logger.Error("Error in GetAssetLicenseData ", ex);
                response.Status = new AdapterStatus((int)AdapterStatusCode.Error, AdapterStatusCode.Error.ToString());
            }

            return response;
        }

        public DrmAdapterResponse GetDeviceLicenseData(int adapterId, int partnerId, string userId, string udid, string deviceFamily, int deviceBrandId, string ip, long timeStamp, string signature)
        {
            var response = new DrmAdapterResponse() { Status = new AdapterStatus() { Code = (int)AdapterStatusCode.Error } };
            _Logger.InfoFormat("start GetDeviceLicenseData");

            try
            {
                // validate signature
                var settingsConcat = string.Concat(adapterId, userId, udid, ip, timeStamp, signature);
                var isSignatureValid = ValidateSignature(adapterId, signature, settingsConcat, timeStamp);
                if (!isSignatureValid)
                {
                    response.Status = CommonResponses.SignatureMismatch;
                    return response;
                }
                _AdapaterConfiguration = new AdapaterConfiguration(_Logger, partnerId, adapterId);
                // check configurations
                if (_AdapaterConfiguration.IsConfigurationSet(adapterId) == false)
                {
                    _Logger.ErrorFormat("GetDeviceLicenseData : No Configuration Found");
                    response.Status = CommonResponses.NoConfigurationFound;
                    return response;
                }

                _PhoenixWrapper = new PhoenixWrapper(_AdapaterConfiguration, _Logger);
                _DRMLogic = new DRMHelper(_AdapaterConfiguration, _Logger, _PhoenixWrapper);

                //builds the AUTHN token
                if (_AdapaterConfiguration.DrmSettings == null)
                {
                    _Logger.Error("Error in GetDeviceLicenseData: drmSettings is null. please Set Configuration.");
                    response.Status = CommonResponses.Error;
                    return response;
                }

                _Logger.DebugFormat("start VF.DRMLogic.CreateLiveAuthToken DrmSettings.Kid:[{0}], DrmSettings.AuthTTL:[{1}], udid, DrmSettings.AuthNagraSecret:[{0}]", _AdapaterConfiguration.DrmSettings.Kid, _AdapaterConfiguration.DrmSettings.AuthTTL, udid, _AdapaterConfiguration.DrmSettings.AuthNagraSecret);
                var authToken = _DRMLogic.CreateLiveAuthToken(_AdapaterConfiguration.DrmSettings.Kid, _AdapaterConfiguration.DrmSettings.AuthTTL, udid, _AdapaterConfiguration.DrmSettings.AuthNagraSecret);
                _Logger.DebugFormat("generated token: [{0}]", authToken);

                response.Status = CommonResponses.OK;
                response.Data = authToken;

                return response;
            }
            catch (Exception ex)
            {
                _Logger.Error("Error in GetDeviceLicenseData", ex);
                response.Status = new AdapterStatus((int)AdapterStatusCode.Error, AdapterStatusCode.Error.ToString());
            }

            return response;
        }

        private bool ValidateSignature(int adapterId, string signature, params object[] paramsToValidate)
        {
            var _Secret = Utils.GetAppSecret(adapterId);
            if (string.IsNullOrEmpty(_Secret))
                _Logger.ErrorFormat("Couldn't find application secret for adapter ID: {0}", adapterId);

            var _ValidateSignature = ConfigurationManager.AppSettings["validateSignature"];

            if (string.IsNullOrEmpty(_ValidateSignature)) { return false; }
            if (_ValidateSignature.Equals("false", StringComparison.InvariantCultureIgnoreCase)) { return true; }

            var paramsConcat = string.Concat(paramsToValidate);
            var isSignatureValid = EncryptionUtils.IsSignatureValid(paramsConcat, signature, _Secret);
            if (!isSignatureValid) _Logger.Debug($"Signature mismatch! Following params:{string.Join(",", paramsToValidate)}, with give signature: {signature} and secret: {_Secret}");
            return isSignatureValid;
        }

        private DefaultUsageRule LookupOPLAndCreateDefaultUsageRules(string externalFileId, string assetId, AssetType assetType)
        {
            var policyName = (assetType == AssetType.RECORDING)
                ? Database.GetPolicyName(_AdapaterConfiguration.DrmSettings.GroupId, externalFileId)
                : GetRecordingPolicyName(assetId);



            //find policy Name value in Dictionary if not found use 0 which should be set to "default"
            var oplDictValue = _AdapaterConfiguration.DrmSettings.OplDictionary.TryGetValueSafe(policyName, 0);

            _Logger.Info($"Opl dictionary value is: [{oplDictValue}]");

            return _AdapaterConfiguration.DrmSettings.DefaultUsageRules.ElementAt(oplDictValue);
        }

        private string GetRecordingPolicyName(string linearMediaAssetId)
        {
            var asset = _PhoenixWrapper.GetAsset(linearMediaAssetId);
            var linearUsageRulesTag = asset.Tags.TryGetValueSafe("usageRulesProfiled")?.Objects?.FirstOrDefault();
            if (linearUsageRulesTag?.Value == null) { return "Default"; };

            return linearUsageRulesTag?.Value;

        }

        private long GetPPVFullLifecycleDuration(string fileId, int userId)
        {
            try
            {
                //The DRM adapter will set EndDate to now + the largest item left view lifecycle of all the PPV modules to which the HH is still entitled.

                var assetContext = _PhoenixWrapper.GetAssetContext(fileId, userId);
                _Logger.Debug($"The viewLifeCycle for fileID:[{fileId}] and userId:[{userId}] viewLifeCycle=[{assetContext.ViewLifeCycle}] FullLifeCycle=[{assetContext.ViewLifeCycle}]");

                var viewLifeCycleTs = TimeSpan.Parse(assetContext.ViewLifeCycle);
                var fullLifeCycleTs = TimeSpan.Parse(assetContext.FullLifeCycle);

                //if leftView lifecycle is 0 => set End Date to be Full lifecycle 
                return (viewLifeCycleTs.TotalSeconds > 0) ? (long)viewLifeCycleTs.TotalSeconds : (long)fullLifeCycleTs.TotalSeconds;
            }
            catch (Exception ex)
            {
                _Logger.Error("Error in SetPPVEndDate ", ex);
                return -1;
            }
        }

        private long GetProductEndDateForFileId(string assetId, string recordingId, long fileId, int userId, ContextType context, AssetType assetType)
        {
            var licenseDurationSec = context == ContextType.DOWNLOAD ?
                GetDownloadProductEndDate(assetId, recordingId, userId, assetType) : GetTvodSVodLicenseDuration(fileId, userId);

            var productEndDate = Utils.GetCurrentEpochTime() + licenseDurationSec;

            return productEndDate;
        }

        private long GetTvodSVodLicenseDuration(long fileId, int userId)
        {
            _Logger.Debug($"Starting GetTvodSVodLicenseDuration with fileId:[{fileId}] and userId:[{userId}]");
            long licenseDuration = 0;
            var product = _PhoenixWrapper.GetProductOfFile(fileId, userId);

            if (product.ProductType == TransactionType.SUBSCRIPTION)
            {
                //license duration = 6 hours
                licenseDuration = _AdapaterConfiguration.DrmSettings.ClmLicenseSVOD;
            }
            else if (product.ProductType == TransactionType.PPV)
            {
                //update ppv end date to be item left view lifecycle
                licenseDuration = GetPPVFullLifecycleDuration(fileId.ToString(), userId);
            }
            else
            {
                throw new Exception($"GetTvodSVodLicenseDuration > Could not determine relevant product type, supported only PPV or SUBSCRIPTION for fileId:[{fileId}] and userId:[{userId}]");
            }

            return licenseDuration;
        }

        private int GetDownloadProductEndDate(string assetId, string recordingId, int userId, AssetType assetType)
        {
            _Logger.Debug($"Starting GetDownloadProductEndDate: assetId[{assetId}]");
            var isDownloadable = assetType == AssetType.RECORDING ?
                _TVPapiWrapper.CheckIfRecordingAssetIsDownloadable(recordingId, userId.ToString())
                : _PhoenixWrapper.CheckIfAssetIsDownloadable(assetId);

            if (!isDownloadable)
            {
                if (assetType == AssetType.RECORDING)
                {
                    throw new Exception($"The  recordingId:[{recordingId}] dose not have EPG_TAGS['genre'] containing d_g=1 tag. download is forbidden");
                }

                throw new Exception($"The asset:[{assetId}] does not have an isDownloadable=1 meta. download is forbidden");
            }

            return _AdapaterConfiguration.DrmSettings.ClmLicenseDownloadContent;
        }
    }
}
