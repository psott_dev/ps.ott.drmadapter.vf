﻿using KLogMonitor;
using PS.OTT.Config.Repository;
using PS.OTT.DRMAdapter.VF.Models;
using System;
using System.Reflection;
using System.Web;

namespace PS.OTT.DRMAdapter.VF.Helper
{
    public class AdapaterConfiguration
    {
        private readonly KLogger _Logger;
        private readonly CouchBaseConfigRepository _ConfigManager;

        private DRMSettings _DrmSettings;
        public DRMSettings DrmSettings => _DrmSettings ?? (_DrmSettings = _ConfigManager.GetConfigurationObject<DRMSettings>());

        public AdapaterConfiguration(KLogger logger, int partnerId, int adapterId)
        {
            _Logger = logger;
            _ConfigManager = new CouchBaseConfigRepository("VFDRMAdapter_" + partnerId.ToString()+"_"+adapterId.ToString(), new ConfigRepoLogger());
        }

       
        public void SetDrmSettings(object drmSettings, int adapterId)
        {
            try
            {
                _ConfigManager.SaveConfiguration(drmSettings);
            }
            catch(Exception e)
            {
                _Logger.Error($"Failed to Save DRM settings. ex: {e.Message}");
            }
        }

        public bool IsConfigurationSet(int adapterId)
        {
            try
            {
                var config = _ConfigManager.GetConfigurationObject<DRMSettings>();
                return config != null;
            }
            catch (Exception ex)
            {
                _Logger.Warn($"Could not find cached configuration", ex);
                return false;
            }
        }
    }

    public class ConfigRepoLogger : IConfigurationRepositoryLogger
    {
        private static readonly KLogger _Logger = new KLogger(MethodBase.GetCurrentMethod().DeclaringType.ToString());

        public void Error(string message, Exception ex = null)
        {
            _Logger.Error(message, ex);
        }

        public void Warn(string message, Exception ex = null)
        {
            _Logger.Error(message, ex);
        }

        public void Info(string message)
        {
            _Logger.Info(message);
        }
    }
}