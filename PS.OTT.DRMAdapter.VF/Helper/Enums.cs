﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PS.OTT.DRMAdapter.VF.Helper
{
   
    public enum eDocumentType
    {
        Configuration,
        Transaction,
        TokenToTransactionMapping
    }

    public enum OPLid
    {
        DEFAULT,
        UHDLEV1
    }
    public enum HeaderParams
    {
        Tvp,
        Alg,
        kid,
    }

    public enum PayloadParams
    {
        Tvp,
        Ver,
        Exp,
        publicDeviceId,
        device,
        contentRights,
        contentId,
        start,
        end,
        minLevel,
        secureMediaPathActivation,
        hdcp,
        hdcpType,
        unprotectedDigitalOutput,
        digitalOnly,
        

    }

    
}