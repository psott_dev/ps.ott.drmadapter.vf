﻿using KLogMonitor;
using System;
using System.Linq;
using System.Reflection;
using System.Web;

namespace PS.OTT.DRMAdapter.VF.Helper
{
    public static class Database
    {
        private static readonly KLogger _Logger = new KLogger(MethodBase.GetCurrentMethod().DeclaringType.ToString());

        public static string GetPolicyName(int groupId, string externalFileId)
        {
            var db = new DBService();
            try
            {
                var results = db.ExecuteProcedure<string>("drm.dbo.Get_PolicyName", new {GroupID = groupId, ExternalFileID = externalFileId}, userPrefix: false);
                var policyName = results.FirstOrDefault();

                
                _Logger.InfoFormat("Policy name is " + policyName);
                return policyName;
            }
            catch (Exception ex)
            {
                _Logger.Error($"Error in GetPolicyName. groupId: {groupId}, externalFileID: {externalFileId}", ex);
                throw;
            }
        }
    }
}