﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using Dapper;
using KLogMonitor;
using PS.OTT.Core.ExternalApiManager.Helpers;

namespace PS.OTT.DRMAdapter.VF.Helper
{
    public class DBService
    {
        private static readonly KLogger _Logger = new KLogger(MethodBase.GetCurrentMethod().DeclaringType.ToString());
        private readonly string _ConnectionString;
        private readonly string _VersionPrefix;

        public DBService()
        {
            _ConnectionString = TCMClient.Settings.Instance.GetValue<string>("CONNECTION_STRING");
            // Remove the ODBC drive as done by BE in ODBCWrapper
            _ConnectionString = _ConnectionString.Replace("Driver={SQL Server};", "");
            _VersionPrefix = TCMClient.Settings.Instance.GetValue<string>("DB_Settings.prefix");
        }

        /// <summary>
        /// Executes a stored procedure
        /// </summary>
        /// <param name="spName">Name without prefix of version</param>
        /// <param name="parameters">an object that will be translated to params, properties should match the SP params.</param>
        /// <param name="commandTimeout">timeout threshold for execution</param>
        /// <param name="userPrefix">weather to use the version prefix or run a versionless SP</param>
        /// <typeparam name="T">Return object type to be mapped by Dapper</typeparam>
        public IEnumerable<T> ExecuteProcedure<T>(string spName, object parameters, int? commandTimeout = null, bool userPrefix = false)
        {
            using (var connection = new SqlConnection(_ConnectionString))
            {
                var prefix = userPrefix ? $"__{_VersionPrefix}__" : "";
                var spNameWithPrefix = $"{prefix}{spName}";
                _Logger.Debug($"Executing Stored Procedure: [{spNameWithPrefix}], with params:[{parameters.ToJsonString()}]");
                var results = connection.Query<T>(spNameWithPrefix, parameters, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);
                _Logger.Debug($"Completed Stored Procedure: [{spNameWithPrefix}], results:[{results.ToJsonString()}]");

                return results;
            }
        }

        public IEnumerable<T> Query<T>(string query, object parameters, int? commandTimeout = null)
        {
            using (var connection = new SqlConnection(_ConnectionString))
            {
                _Logger.Debug($"Executing Query: [{query}], with params:[{parameters.ToJsonString()}]");
                var results = connection.Query<T>(query, parameters, commandTimeout: commandTimeout);
                _Logger.Debug($"Completed Query: [{query}], result:[{results.ToJsonString()}]");

                return results;
            }
        }
    }
}