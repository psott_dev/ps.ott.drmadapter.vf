﻿using System;
using System.Collections.Generic;
using DRMAdapter;
using KLogMonitor;
using PS.OTT.DRMAdapter.VF.BECalls;
using PS.OTT.DRMAdapter.VF.Models;

namespace PS.OTT.DRMAdapter.VF.Helper
{
    public class DRMHelper
    {
        private readonly KLogger _Logger;
        private readonly AdapaterConfiguration _Configuration;
        private PhoenixWrapper _PhoenixWrapper;

        public DRMHelper(AdapaterConfiguration adapaterConfiguration, KLogger logger, PhoenixWrapper phoenixWrapper)
        {
            _Configuration = adapaterConfiguration;
            _Logger = logger;
            _PhoenixWrapper = phoenixWrapper;
        }

        public string CreateLiveAuthToken(string kid, int authTTL, string udid, string nagraAuthSecret)
        {
            string resToken = null;
            var authToken = new AuthTokenParams()
            {
                header = InitializeHeaders(kid),
                claims = InitializeLiveClaims(authTTL, udid)
            };
            var extraHeader = new Dictionary<string, object> { { "kid", kid } };
            resToken = CreateTokenSignature(extraHeader, authToken.claims, nagraAuthSecret);
            return resToken;
        }

        private Header InitializeHeaders(string kid)
        {
            var headers = new Header();
            headers.typ = "JWT";
            headers.alg = "HS256";
            headers.kid = kid;

            return headers;
        }

        private Claims InitializeLiveClaims(int authTTL, string udid)
        {
            var claim = new Claims
            {
                typ = "DevAuthN",
                ver = "1.0",
                exp = authTTL + Utils.GetCurrentEpochTime(),
                deviceId = udid
            };

            return claim;
        }


        public string CreateVODCLMToken(string udid, string externalFileId, long endTime, DefaultUsageRule usageRules, ContextType context)
        {
            var kid = _Configuration.DrmSettings.Kid;
            var clmTTL = _Configuration.DrmSettings.ClmTTL;
            var nagraClmSecret = _Configuration.DrmSettings.ClmNagraSecret;

            string resToken = null;
            var clmToken = new CLMTokenParams()
            {
                header = InitializeHeaders(kid),
                claims = InitializeVODClaims(udid, externalFileId, endTime, usageRules, clmTTL, context),
            };
            var extraHeader = new Dictionary<string, object> { { "kid", kid } };
            resToken = CreateTokenSignature(extraHeader, clmToken.claims, nagraClmSecret);
            return resToken;
        }

        private CLMClaims InitializeVODClaims(string udid, string externalFileId, long endTime, DefaultUsageRule usageRules, int clmTTL, ContextType context)
        {
            try
            {
                var clmClaim = new CLMClaims
                {
                    typ = "ContentAuthZ",
                    ver = "1.0",
                    exp = Utils.GetCurrentEpochTime() + clmTTL,
                    device = new Device() { deviceId = udid },
                    contentRights = new List<ContentRight>()
                };

                //1 minute from time of token creation
                var contentRight = new ContentRight()
                {
                    contentId = externalFileId,
                    start = Utils.GetCurrentEpochTime(),
                    end = endTime,
                    defaultUsageRules = usageRules,
                };
                if (context == ContextType.DOWNLOAD)
                {
                    contentRight.storable = true;
                }

                clmClaim.contentRights.Add(contentRight);


                return clmClaim;
            }
            catch (Exception ex)
            {
                _Logger.Error("Error in InitializeVODClaims: ", ex);
                return null;
            }
        }


        private string CreateTokenSignature(IDictionary<string, object> extraHeaders, object token, string secretKey)
        {
            var JWTtoken = JWT.JsonWebToken.Encode(extraHeaders, token, secretKey, JWT.JwtHashAlgorithm.HS256);
            return JWTtoken;
        }
    }
}