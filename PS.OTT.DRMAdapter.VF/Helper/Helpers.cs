﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using KLogMonitor;
using Newtonsoft.Json;

namespace PS.OTT.DRMAdapter.VF.Helper
{
    public static class Helpers
    {
        private static readonly KLogger _Logger = new KLogger(MethodBase.GetCurrentMethod().DeclaringType.ToString());

        public static T TryGetValueSafe<TK, T>(this IDictionary<TK, T> sourceDictionary, TK key, T defaultReturnValue = default(T))
        {
            var returnVal = defaultReturnValue;
            sourceDictionary.TryGetValue(key, out returnVal);
            return returnVal;
        }

        public static void Merge<TKey, TValue>(this IDictionary<TKey, TValue> me, IDictionary<TKey, TValue> merge)
        {
            foreach (var item in merge)
            {
                me[item.Key] = item.Value;
            }
        }

        public static string ToJson(this object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj);
            }
            catch (Exception e)
            {
                var msg =  $"Error While deserializer object of type {obj.GetType()}";
                _Logger.Error(msg, e);
                return msg;
            }
        }
    }
}