﻿using AdapaterCommon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PS.OTT.DRMAdapter.VF.Helper
{
    public class CommonResponses
    {
        private const string OK_STR = "OK";
        private const string ERROR = "Internal error";
        private const string SIGNATURE_MISMATCH = "Signature does not match";
        private const string CONFIGURATION_NOT_FOUND = "Configuration not found";

        public static readonly AdapterStatus OK = new AdapterStatus((int)AdapterStatusCode.OK, OK_STR);
        public static readonly AdapterStatus SignatureMismatch = new AdapterStatus((int)AdapterStatusCode.SignatureMismatch, SIGNATURE_MISMATCH);
        public static readonly AdapterStatus NoConfigurationFound = new AdapterStatus((int)AdapterStatusCode.NoConfigurationFound, CONFIGURATION_NOT_FOUND);
        public static readonly AdapterStatus Error = new AdapterStatus { Code = (int)AdapterStatusCode.Error };
    }
}