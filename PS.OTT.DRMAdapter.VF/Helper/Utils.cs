﻿using AdapaterCommon.Models;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PS.OTT.DRMAdapter.VF.Helper
{
    internal static class Utils
    {
        public static long GetExpirationEpochTimeSec(int minutes)
        {
            //X minute from time of token creation
            var t = DateTime.UtcNow.AddMinutes(minutes) - new DateTime(1970, 1, 1);
            var secondsSinceEpoch = (long)t.TotalSeconds;
            return secondsSinceEpoch;
        }

        public static long GetCurrentEpochTime()
        {
            var t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            var secondsSinceEpoch = (long)t.TotalSeconds;
            return secondsSinceEpoch;
        }

        public static DateTime FromUnixTime(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static long FromDateTimeToUnix(DateTime date)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var diff = date.ToUniversalTime() - origin;
            return (long)diff.TotalSeconds;
        }

        public static string GetAppSecret(int adapterId)
        {
            var appSecret = ConfigurationManager.AppSettings[string.Format("appSecret_{0}", adapterId)];

            //if (string.IsNullOrEmpty(appSecret))
            //    log.ErrorFormat("Couldn't find application secret for adapter ID: {0}", adapterId);

            return appSecret;
        }


    }
}