﻿using KLogMonitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace PS.OTT.DRMAdapter.VF
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            // set monitor and log configuration files
            KMonitor.Configure("log4net.config", KLogEnums.AppType.WCF);
            KLogger.Configure("log4net.config", KLogEnums.AppType.WCF);
            TCMClient.Settings.Instance.Init();
        }
    }
}